<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
</head>
   
<?php 
include("includes/database.php"); 
include("includes/header.php"); 
if (isset($_SESSION['email'])){ 
 
function displayAll($db){     // function used for the admin display (every ticket)
   $q = $db -> query('SELECT count(id) FROM ticket ');   //count the number of tickets
   $nbTickets = $q->fetch();
   for($i=1;$i<=$nbTickets[0];$i++) // for every ticket
   {
      $q2 = $db -> query("SELECT * FROM ticket WHERE id='$i'");                   
      while($getinformations=$q2 -> fetch()) //declare a variable that gets the fetched query
      {?>
         <tr>
            <td><?php echo($getinformations['id']);?></td>
            <td><?php echo($getinformations['datet']);?></td>
            <td><?php echo($getinformations['login']);?></td>
            <td><?php echo($getinformations['subject']);?></td>
            <td><?php echo($getinformations['description']);?></td>
            <td><?php echo($getinformations['prio']);?></td>
            <td><?php echo($getinformations['sector']);?></td>
            <td><?php echo($getinformations['status']."<br/>");?></td>
         </tr>
         <?php }}
}

function displayUser($db){    //same function but the where condition is changing : now we use the
   $nick=$_SESSION['nick'];   // session variable "nick" to get the tickets we want to display (we're doing this to display tickets emitted by the user connected)
   $q = $db -> query("SELECT count(id) FROM ticket WHERE login='$nick'"); 
   $nbTickets=$q->fetch();
   for($i=1;$i<$nbTickets[0];$i++)
   {
      $q2 = $db -> query("SELECT * FROM ticket WHERE login='$nick'");  
      while($getinformations=$q2 -> fetch())
      {?>
         <tr>
            <td><?php echo($getinformations['id']);?></td>
            <td><?php echo($getinformations['datet']);?></td>
            <td><?php echo($getinformations['login']);?></td>
            <td><?php echo($getinformations['subject']);?></td>
            <td><?php echo($getinformations['description']);?></td>
            <td><?php echo($getinformations['prio']);?></td>
            <td><?php echo($getinformations['sector']);?></td>
            <td><?php echo($getinformations['status']."<br/>");?></td>
         </tr>
            <?php
            }}}
?>

<h2>Display of every ticket<?php if($_SESSION['role'] == "user"){echo " you have submitted";} ?>: </h2>
<table class="table">
   <thead>
      <tr>
         <th>id</th>
         <th>datet</th>
         <th>login</th>
         <th>subject</th>
         <th>description</th>
         <th>prio</th>
         <th>sector</th>
         <th>state</th>
      </tr>
   </thead>
   <tbody>
      <?php $nick = $_SESSION['nick'];
            if($_SESSION['role'] == "admin"){  // if an administrator is connected
               displayAll($db);
            }else if ($_SESSION['role'] == "user"){   // if a basic user is connected
               displayUser($db);
            }?>
      </tbody>
</table>
</div>
<?php } else {
   echo "LOG IN";
}?>