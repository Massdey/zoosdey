<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/smallpages.css">
</head>

<?php session_start();
include "includes/database.php";

function testAndDisplay($db){

    if(isset($_POST['connexion']))  // if connection informations were sent 
    {
        extract($_POST);            // extract form inputs

        if(!empty($email) && !empty($password))         // if email and password have been filled in the form
        {
            $q = $db -> prepare ("SELECT * FROM users WHERE email = :email");            // check if the email exists in the database
            $q -> execute (['email' => $email]);
            $result = $q -> fetch();   
            if( $result == true )                        // if the account exists
            {  
                $hashpass = $result['password'];        // get the hashed password in the database
                if(password_verify($password,$hashpass))   
                {
                    $_SESSION['email'] = $email;
                    $_SESSION['nick'] = $result['nick'];
                    $_SESSION['role'] = $result['role'];
                    echo " Now connected as " . $result['nick']; 
                    ?><br/><a href="index.php">Index</a><?php

                }// in this part, we have stored the email and the password in session variables
                else
                {   
                    echo "The password you've entered is not linked to that account ";
                    ?><br/><a href="index.php">Index</a><?php
                }
            }else{
            echo "The email you've entered is not matching any existing account ";
            ?><br/><a href="index.php">Index</a><?php
            }
        }else{
        echo "One mandatory field hasn't been filled ";
        ?><br/><a href="index.php">Index</a><?php
        }
    }else{
    echo "The connection form hasn't been filled, please go back to authentification page";
    ?><br/><a href="index.php">Index</a><?php
    }
}

testAndDisplay($db);
?> 

