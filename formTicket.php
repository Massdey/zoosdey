<head>
    <link rel="stylesheet" href="css/ticket.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head> 

<?php 

function printForm(){
    include("includes/header.php"); 
    if (isset($_SESSION['email'])){ //if the user is connected
    ?>
    <form method="post" action="recupTicket.php" class="form" name="ticketForm">
    <h3> Ticket </h3>
    <div class="form-group">
        Enter the title of your problem<br/>
        <input type="text" name="title"> </input><br/>
    </div>
    <div class="form-group">
        <textarea name="description"> Enter a comprehensive description of your problem</textarea><br/>
    </div>
    <div class="form-group"></div>
        priority level:<br/><select class="form-select" aria-label="Default select example" name="danger">
            <option selected>--please choose a level of danger--</option>
            <option value="Low"> Low danger </option>
            <option value="Medium"> Medium danger </option>
            <option value="High"> High danger </option>
        </select><br/>
    </div>
    <div class="form-group"></div>
        status: <br/><select class="form-select" aria-label="Default select example" name="status">
            <option selected>--please choose a status--</option>
            <option value="happening"> In progress </option>
            <option value="happened"> Already happened </option>
            <option value="willhappen"> Will happen  </option>
        </select><br/><br/>
    </div>
    <div class="form-group">
        which zoo sector is concerned: <br/><select class="form-select" aria-label="Default select example" name="sector">
            <option value="">--please choose a zoo sector--</option>
            <option value="Africa"> Africa's lands</option>
            <option value="Asia"> Asia's lands </option>
            <option value="Tropics"> Tropics </option>
            <option value="Southam">South america's lands</option>
        </select><br/><br/>
    </div>
    <div class="form-group">
        <h3> Your login </h3>
        enter your nickname:<input type="text" placeholder="your nick" name="nick"><br/>
        enter your password:<input type="password" placeholder="your password" name="pass"><br/>
        <button type="submit" class="btn btn-primary" name="bouton">Submit</button>
    </div>
  </form>
</body>
<?php } else {
   ?> <p style="padding-top:13%;text-align:center; font-family:sans-serif; font-size:6vh; color:white;">LOG IN</p>
   <?php 
}
}
?>

<body>
<?php printForm(); ?>
</body>
